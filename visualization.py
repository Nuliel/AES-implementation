#!/usr/bin/python3
import matplotlib.pyplot as plt
import random
from math import sqrt
import struct
import time

user_input = input("Choose an option:\n"
                   "[1] traces\n"
                   "[2] correlation (we need to run ./cpa before)\n")

while user_input != "1" and user_input != "2":
    user_input = input("Select a good option\n")

if user_input == "1":
    user_input = input("Choose an option:\n"
                       "[1] software knwon\n"
                       "[2] software unknwon\n"
                       "[3] hardware knwon\n"
                       "[4] hardware unknwon\n"
                       "[5] software knwon countermeasure 0\n"
                       "[6] software knwon countermeasure 1\n"
                       "[7] software knwon countermeasure 2\n"
                       "[8] software unknwon countermeasure 0\n"
                       "[9] software unknwon countermeasure 1\n"
                       "[10] software unknwon countermeasure 2\n")

    while user_input != "1" and user_input != "2" and user_input != "3" and user_input != "4" and user_input != "5" and user_input != "6" and user_input != "7" and user_input != "8" and user_input != "9" and user_input != "10":
        user_input = input("Select a good option\n")

    if user_input == "1":
        goto = "./data/software_traces_k_known/traces.bin"
    elif user_input == "2":
        goto = "./data/software_traces_k_unknown/traces.bin"
    elif user_input == "3":
        goto = "./data/hardware_traces_k_known/traces.bin"
    elif user_input == "4":
        goto = "./data/hardware_traces_k_unknown/traces.bin"
    elif user_input == "5":
        goto = "./data/new_traces/software_traces_k_known_countermeasure_0/traces.bin"
    elif user_input == "6":
        goto = "./data/new_traces/software_traces_k_known_countermeasure_1/traces.bin"
    elif user_input == "7":
        goto = "./data/new_traces/software_traces_k_known_countermeasure_2/traces.bin"
    elif user_input == "8":
        goto = "./data/new_traces/software_traces_k_unknown_countermeasure_0/traces.bin"
    elif user_input == "9":
        goto = "./data/new_traces/software_traces_k_unknown_countermeasure_1/traces.bin"
    elif user_input == "10":
        goto = "./data/new_traces/software_traces_k_unknown_countermeasure_2/traces.bin"

    with open(goto, "rb") as f:
        bsample = f.read(4)
        btraces = f.read(4)
        sample = int.from_bytes(bsample, byteorder='little')
        traces = int.from_bytes(btraces, byteorder='little')
        print(f"sample: {sample}, traces : {traces}")

        it = []
        res = []
        for j in range(sample):
                it.append(j)
                res.append(struct.unpack('f', f.read(4)))

        plt.plot(it, res, '#0080ff')
        plt.show()

else:
    file_name = ""
    with open("./config.cpa", "r") as f:
        c = f.read(1)
        while c != '\n':
            file_name += c
            c = f.read(1)
        
        if file_name.find("hardware") != -1:
            nb_sample = 3252
        else:
            nb_sample = 9996

    end = 0
    counter_sample = 0
    counter_traces = 0
    counter_key = 0
    tot = []
    curr = []
    print("Processing....")
    with open("./correlation.cpa", "rb") as f:
        while end == 0:
            c = f.read(8)
            if c != b'':
                curr.append(float(c))
            counter_sample += 1
            if counter_sample == nb_sample:
                c = f.read(1)
                counter_traces += 1
                counter_sample = 0
                tot.append(curr)
                curr = []
            if counter_traces == 1000:
                end = 1

    it = [i for i in range(nb_sample)]

    print("Done")
    for i in range(1000):
        plt.plot(it, tot[i], '#0080ff')

    plt.show()