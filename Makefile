CFLAGS=-std=c11 -Wall -Wextra -g -O3
# CPPFLAGS=-I../include -DDEBUG
LDFLAGS=-lm
.PHONY: all clean help

all: cpa

# aes: aes.o
# 	$(CC) $(CFLAGS) -o $@ $^ $(LDFLAGS)

cpa: cpa.o gnuplot_i.o reader.o stats.o aes.o
	$(CC) $(CFLAGS) -o $@ $^ $(LDFLAGS)

aes.o: aes.c aes.h
	$(CC) $(CFLAGS) $(CPPFLAGS) -c $<

cpa.o: cpa.c gnuplot_i/src/gnuplot_i.h reader.h stats.h
	$(CC) $(CFLAGS) $(CPPFLAGS) -c $<

gnuplot_i.o: 
	cd gnuplot_i/ && $(MAKE)
	@cp -f gnuplot_i/gnuplot_i.o ./

reader.o: reader.c reader.h
	$(CC) $(CFLAGS) $(CPPFLAGS) -c $<

stats.o: stats.c stats.h
	$(CC) $(CFLAGS) $(CPPFLAGS) -c $<

clean: 
	@rm -f *.o aes cpa *~ mean.png deviation.png
	@rm -f peda-session-*
	@rm -f ./data_plot/*
	@cd gnuplot_i/ && $(MAKE) clean

help : 
	@echo "Targets: all, clean, help"
	@echo "	all: compile the project"
	@echo "	clean: remove .o and compiled program"
	@echo "	help: display this help"
