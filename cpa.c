#include <err.h>
#include <errno.h>
#include <getopt.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "aes.h"
#include "reader.h"
#include "stats.h"
#include "gnuplot_i/src/gnuplot_i.h"

#define NPOINTS 50

/*8 bit popcount*/
const uint8_t m1 = 0x55;
const uint8_t m2 = 0x33;
const uint8_t m3 = 0x0F;

typedef enum
{
    no_mode,
    mode_first,
    mode_last
} mode_selection;

void print_help(FILE *file_out)
{
    int ret = fprintf(file_out,
                      "Usage: cpa [--config FILE]\n"
                      "       cpa [-t | -p | -c FILE]\n"
                      "       --config FILE    specify a config file, default is\n"
                      "                        config.cpa\n"

                      "Following options are not compatible with --config:\n"
                      "       -t FILE          specify a file containing traces\n"
                      "       -p FILE          specify a file containing plaintexts\n"
                      "       -c FILE          specify a file containing ciphertexts\n"
                      "       -h               display this help\n");
    if (ret < 0)
    {
        errx(EXIT_FAILURE, "error: cannot write to file");
    }
}

/**
 * @brief Config file parser
 * 
 * @param conf: pointer to the config file 
 * @param traces: pointer to a string 
 * @param plaintext: pointer to a string
 * @param ciphertext: pointer to a string
 */
void config_parser(const char *conf, char *traces, char *plaintext,
                   char *ciphertext)
{
    FILE *fd;
    if ((fd = fopen(conf, "r")) == NULL)
    {
        errx(EXIT_FAILURE, "error: cannot open file %s : %s\n", conf,
             strerror(errno));
    }

    int c;
    int i = 0;
    while ((c = fgetc(fd)) != '\n')
    {
        traces[i] = c;
        i++;
    }
    traces[i] = '\0';

    i = 0;
    while ((c = fgetc(fd)) != '\n')
    {
        plaintext[i] = c;
        i++;
    }
    plaintext[i] = '\0';

    i = 0;
    c = fgetc(fd);
    while (c != EOF && c != '\n')
    {
        ciphertext[i] = c;
        i++;
        c = fgetc(fd);
    }
    ciphertext[i] = '\0';
    fclose(fd);
}

/**
 * @brief Compute Hamming weight of a and b
 * 
 * @param a 
 * @param b
 */
byte pop_count(uint8_t x)
{
    x = (x & m1) + ((x >> 1) & m1);
    x = (x & m2) + ((x >> 2) & m2);
    x = (x & m3) + ((x >> 4) & m3);
    return x;
}

/**
 * @brief Compute Hamming distance of a and b
 * 
 * @param a 
 * @param b
 */
byte hamming_dist(uint8_t a, uint8_t b)
{
    return pop_count(a ^ b);
}

/**
 * @brief Compute Hamming weight after SBOX in first round
 * 
 * @param clear byte of clear
 * @param key byte of master key
 */
byte mode0_first_round(byte clear, byte key)
{
    return pop_count(sBox[clear ^ key]);
}

/**
 * @brief Compute Hamming distance between the ciphertext and the
 * input of the sbox at the last round.
 *   return HD (inv_Sbox (target_ciphertext^key), overwrite_value)
 * /!\ the given 'target_ciphertext' and the 'overwrite_value' should be
 * well choosen as explained in the video. 
 * @param target_ciphertext
 * @param overwrite_value
 * @param key guessed key value
 */
byte mode1_last_round(byte target_ciphertext, byte overwrite_value, byte key)
{
    return hamming_dist(inverse_sBox[target_ciphertext ^ key], overwrite_value);
}

/**
 * @brief compute the leakage model at the first round (HW of the
 * output of the SBox at the first round) for the target byte of k0.
 *  return an array of dimension 256 (nb of possible guess for the
 * key byte) \times number of traces.
 * @param plain: plaintext
 * @param target_byte: target byte of k0 
 * @param size_N: number of traces
 */

float **leakage_model_first_round(byte **plain, byte target_byte, int size_N)
{
    float **model;
    model = malloc(size_N * sizeof(float *));
    for (int i = 0; i < size_N; i++)
    {
        model[i] = malloc(256 * sizeof(float));
    }
    for (int key = 0; key < 256; key++)
    {
        for (int trace = 0; trace < size_N; trace++)
        {
            model[trace][key] = mode0_first_round(plain[trace][target_byte],
                                                  key);
        }
    }
    return model;
}

/**
 * @brief compute the leakage model at the last round (HD between the
 * input of the SBox at the last round and the ciphertext) for the
 * target byte of k10.
 *  return an array of dimension 256 (nb of possible guess for the
 * key byte) \times number of traces.
 * /!\ it is in this function that you need use an array of translation
 * of indexes for the HD before calling the 'mode1_last_round' function
 * @param cipher: ciphertext 
 * @param target_byte: target byte of k10 
 * @param size_N: number of traces
 */
float **leakage_model_last_round(byte **cipher, byte target_byte, int size_N)
{
    float **model;
    int shift[16] = {0, 5, 10, 15, 4, 9, 14, 3, 8, 13, 2, 7, 12, 1, 6, 11};
    model = malloc(size_N * sizeof(float *));
    for (int i = 0; i < size_N; i++)
    {
        model[i] = malloc(256 * sizeof(float));
    }
    for (int i = 0; i < 256; i++)
    {
        for (int j = 0; j < size_N; j++)
        {
            model[j][i] = mode1_last_round(cipher[j][target_byte],
                                           cipher[j][shift[target_byte]], i) -
                          4;
        }
    }
    return model;
}

/**
 * @brief compute the leakage model at the last round or first round
 *  return an array of dimension 256 (nb of possible guess for the
 * key byte) \times number of traces.
 * @param matrix: ciphertext or plaintext
 * @param target_byte: target byte of k10
 * @param size_N: number of traces
 * @param mode: mode_first or mode_last
 */
float **leakage_model(byte **matrix, byte target_byte, int size_N,
                      const mode_selection mode)
{
    float **model = NULL;
    switch (mode)
    {
    case mode_first:
        model = leakage_model_first_round(matrix, target_byte, size_N);
        break;
    case mode_last:
        model = leakage_model_last_round(matrix, target_byte, size_N);
        break;
    default:
        printf("Round should be 1 or 2\n");
        break;
    }
    return model;
}

/**
 * @brief Plot statistics using gnu plot, reates png files.
 * @param traces: struct containing the traces 
 * @param r: struct containing the statistics
 */
void plot_statistics(struct traces_struct traces, result r)
{
    double *x = malloc(traces.nb_of_samples * sizeof(double));
    for (int i = 0; i < traces.nb_of_samples; i++)
        x[i] = i;

    gnuplot_ctrl *g1 = gnuplot_init();
    gnuplot_ctrl *g2 = gnuplot_init();
    gnuplot_cmd(g1, "set terminal png");
    gnuplot_cmd(g2, "set terminal png");
    gnuplot_setstyle(g1, "lines");
    gnuplot_setstyle(g2, "lines");
    gnuplot_cmd(g1, "set output \"mean.png\"");
    gnuplot_cmd(g2, "set output \"deviation.png\"");
    gnuplot_plot_xy(g1, x, r->mean, traces.nb_of_samples, "mean");
    gnuplot_plot_xy(g2, x, r->deviation, traces.nb_of_samples, "deviation");
    gnuplot_close(g1);
    gnuplot_close(g2);

    free(x);
    free_res(r);

    /* gnuplot_ctrl *g3 = gnuplot_init(); */
    /* gnuplot_cmd(g3, "set terminal png"); */

    /* gnuplot_setstyle(g3, "lines") ; */
    /* gnuplot_cmd(g3, "set output \"corr.png\""); */
    /* gnuplot_close(g3); */
}

/**
 * @brief Write data to plot in a file to plot with matplotlib
 * @param cor: correlation array
 * @param size_d: number of samples per trace
 * @param key_offset: good key
 */
void plot_stats_correlation(double **cor, int size_d, int key_offset)
{
    char file_name[24] = "data_plot";
    char buf[3] = "";
    sprintf(buf, "%0*d", 2, key_offset);
    strncat(file_name, buf, 2);
    strncat(file_name, ".data", 5);
    printf("filename = '%s'\n", file_name);
    FILE *file_out = fopen(file_name, "w");
    for (int j = 0; j < size_d; j++)
        fprintf(file_out, "%f,", cor[key_offset][j]);
    fprintf(file_out, "\n");
    for (int i = 0; i < 256; i++)
    {
        if (i != key_offset)
        {
            for (int j = 0; j < size_d; j++)
            {
                fprintf(file_out, "%f,", cor[i][j]);
            }
            fprintf(file_out, "\n");
        }
    }
    fprintf(file_out, "\n");
    fclose(file_out);
}

/**
 * @brief Build the model matrix
 * @param traces: struct traces_struct
 * @param res_traces: struct result
 * @param plaintext: struct data_struct
 * @param ciphertext: struct data_struct
 * @param key: struct pair
 * @param mode: mode_first or mode_last
 */
void find_key(struct traces_struct traces, result res_traces,
              struct data_struct plaintext,
              struct data_struct ciphertext,
              struct pair *key, const mode_selection mode, int *offset,
              FILE *fd)
{
    for (int i = 0; i < 16; i++)
    {
        float **model = NULL;
        if (mode == mode_first)
            model = leakage_model(plaintext.data, i,
                                      plaintext.nb_of_traces, mode);
        else
            model = leakage_model(ciphertext.data, i,
                                      ciphertext.nb_of_traces, mode);
        double **cor = correlation(traces.traces, model, traces.nb_of_samples,
                                   traces.nb_of_traces, res_traces, offset, fd);
        key[i] = candidate(cor, traces.nb_of_samples, 256);

        //plot_stats_correlation(cor, traces.nb_of_samples, i);

        printf("Best bytes %d: %02x %02x\n", i, key[i].first, key[i].second);
        for (int i = 0; i < 256; i++)
            free(cor[i]);
        for (int i = 0; i < ciphertext.nb_of_traces; i++)
            free(model[i]);
        free(model);
        free(cor);
    }
}

/**
 * @brief Find the key
 * @param plaintext: struct data_struct
 * @param ciphertext: struct data_struct
 * @param key: struct pair
 * @param mode: mode_first or mode_last
 */
byte *verify_key(struct data_struct plaintext, struct data_struct ciphertext,
                 struct pair *key, const mode_selection mode)
{
    uint16_t i = 0;
    bool found = false;
    byte clear[16];
    byte cipher[16];
    byte test_clear[16];
    byte *test_key = malloc(16 * sizeof(byte));

    for (int j = 0; j < 16; j++)
    {
        clear[j] = plaintext.data[0][j];
        cipher[j] = ciphertext.data[0][j];
    }

    while (!found && i != UINT16_MAX)
    {
        /* make test_key */
        for (int k = 0; k < 16; k++)
        {
            if (((i & (1 << k)) >> k) == 0)
            {
                test_key[k] = key[k].first;
            }
            else
            {
                test_key[k] = key[k].second;
            }
        }

        /* make test_clear */
        for (int j = 0; j < 16; j++)
            test_clear[j] = clear[j];

        /* for hardware traces with key at last round, find the master key */
        if (mode == mode_last)
        {
            recoverKey(test_key);
        }

        /* verify the key using first clear and cipher */

        aes(test_clear, test_key);
        found = true;
        for (int j = 0; j < 16; j++)
        {
            if (test_clear[j] != cipher[j])
            {
                found = false;
                break;
            }
        }
        i++;
    }
    if (found)
    {
        recoverKey(test_key);
        return test_key;
    }
    return NULL;
}

int main(int argc, char *argv[])
{
    struct option long_opts[] = {
        {"traces", required_argument, NULL, 't'},
        {"plaintext", required_argument, NULL, 'p'},
        {"ciphertext", required_argument, NULL, 'c'},
        {"first", no_argument, NULL, 'f'},
        {"last", no_argument, NULL, 'l'},
        {"random-delay", no_argument, NULL, 'r'},
        {"config", required_argument, NULL, 'C'},
        {"plot-stats", no_argument, NULL, 'P'},
        {"help", no_argument, NULL, 'h'},
        {NULL, 0, NULL, 0}};

    mode_selection mode = no_mode;
    char traces_f[2048];
    char plaintext_f[2048];
    char ciphertext_f[2048];
    bool conf_file_mode = false;
    bool opt_mode = false;
    bool plot_stats = false;
    bool random_delay = false;
    int optc;
    char config_file[1024];

    while ((optc = getopt_long(argc, argv, "t:p:c:flrC:P:h", long_opts, NULL))
           != -1)
    {
        switch (optc)
        {
        case 't':
            strcpy(traces_f, optarg);
            printf("traces_f == %s \n", traces_f);
            opt_mode = true;
            break;
        case 'p':
            strcpy(plaintext_f, optarg);
            printf("plaintext_f == %s\n", plaintext_f);
            opt_mode = true;
            break;
        case 'c':
            strcpy(ciphertext_f, optarg);
            printf("ciphertext_f == %s\n", ciphertext_f);
            opt_mode = true;
            break;
        case 'C':
            strcpy(config_file, optarg);
            conf_file_mode = true;
            printf("reading from config file: %s \n", config_file);
            //printf(" with arg '%s'\n", optarg);
            break;
        case 's':
            plot_stats = true;
            printf("Enabling statistics plots\n");
            break;
        case 'l':
            if (mode == mode_first)
                errx(EXIT_FAILURE, "Conflit between attack mode\n");
            mode = mode_last;
            break;
        case 'r':
            random_delay = true;
            break;
        case 'f':
            if (mode == mode_last)
                errx(EXIT_FAILURE, "Conflit between attack mode\n");
            mode = mode_first;
            break;
        case 'h':
            print_help(stdout);
            return EXIT_SUCCESS;
        default:
            return EXIT_FAILURE;
            break;
        }
    }
    if (opt_mode && conf_file_mode)
    {
        errx(EXIT_FAILURE, "error: wrong use of arguments see help page\n");
    }
    if (conf_file_mode)
    {
        config_parser(config_file, traces_f, plaintext_f, ciphertext_f);
    }
    /*if (argc == 1)
    {*/
        config_parser("config.cpa", traces_f, plaintext_f, ciphertext_f);
    //}


    printf("%s\n", traces_f);
    struct traces_struct traces = load_traces(traces_f);
    printf("ok\n");
    printf("%s\n", plaintext_f);
    struct data_struct plaintext = load_data(plaintext_f);
    printf("ok\n");
    printf("%s\n", ciphertext_f);
    struct data_struct ciphertext = load_data(ciphertext_f);
    printf("ok\n");

    FILE *file_out = fopen("correlation.cpa", "w");

    if (mode == no_mode)
        mode = mode_first;
    
    int *offset = NULL;
    if (random_delay)
        offset = offset_compute(traces.traces, traces.nb_of_samples,
                                      traces.nb_of_traces);
    result res_traces = stats_compute(traces.traces, traces.nb_of_samples,
                                      traces.nb_of_traces, offset);

    if (plot_stats)
    {
        plot_statistics(traces, res_traces);
        //double **cor = correlation(traces.traces, model, traces.nb_of_samples,
        //                           traces.nb_of_traces, res_traces);
    }

    struct pair key[16];

    find_key(traces, res_traces, plaintext, ciphertext,
             key, mode, offset, file_out);

    byte *found_key = verify_key(plaintext, ciphertext, key, mode);
    if (found_key != NULL)
    {
        fct_debug(found_key);
    }
    else
    {
        printf("No key found!\n");
    }

    fclose(file_out);
    free(found_key);
    free_struct_traces(traces);
    free_struct_data(plaintext);
    free_struct_data(ciphertext);
    free_res(res_traces);
    if (offset != NULL)
        free(offset);

    return EXIT_SUCCESS;
}