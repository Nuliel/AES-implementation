//****************************************************************************//
//                                                                            //
//    Paper used :                                                            //
//                                                                            //
//       - http://www.infosecwriters.com/text_resources/pdf/AESbyExample.pdf  //
//                                                                            //
//       - http://csrc.nist.gov/publications/fips/fips197/fips-197.pdf        //
//                                                                            //
//****************************************************************************//

#include <stdio.h>
#include <stdlib.h>

#include "aes.h"

void fct_debug (byte arr[16])
{
    for (int i = 0; i < 16; i++)
        printf("%02x ", arr[i]);
    printf("\n");
}

/**
 * @brief Multiplication on Galois field
 * 
 * @param a 
 * @param b 
 * @return byte 
 */
byte mult (byte a, byte b)
{
    if (a == 0 || b == 0)
        return 0;
    else if (a == 1)
        return b;
    else if (b == 1)
        return a;

    int res = L[a] + L[b];
    if (res > 0xff)
    {
        res -= 0xff;
    }

    return E[res];
}

/**
 * @brief Apply the S-Box on the state (in place)
 * 
 * @param state 
 */
void byteSub (byte *state)
{
    for (int i = 0; i < 16; i++)
        state[i] = sBox[state[i]];
}

/**
 * @brief Make the XOR (in place) between the state and the expand key
 * 
 * @param state 
 * @param key 
 */
void addRoundKey (byte *state, byte *key)
{
    for (int i = 0; i < 16; i++)
        state[i] = key[i] ^ state[i];
}

/**
 * @brief Make the shift (in place) between the row of the state
 *        
 *        From               To
 * 
 *        1  5   9  13         1   5  9  13
 *        2  6  10  14         6  10 14   2
 *        3  7  11  15        11  15  3   7
 *        4  8  12  16        16   4  8  12
 * 
 * @param state 
 */
void shiftRow (byte *state)
{
    byte temp;
	// Row 2
	temp = state[1];
    state[1] = state[5];
    state[5] = state[9];
    state[9] = state[13];
    state[13] = temp;
	//Row 3
	temp = state[10];
    state[10] = state[2];
    state[2] = temp;
	temp = state[14];
    state[14] = state[6];
    state[6] = temp;
	//Row 4
	temp = state[3];
    state[3] = state[15];
    state[15] = state[11]; 
    state[11] = state[7];
    state[7] = temp;
}

/**
 * @brief Multiplication (in place) between the multiplication matrix and the state
 * 
 *        2  3  1  1             b1  b5  b9   b13
 *        1  2  3  1     x       b2  b6  b10  b14
 *        1  1  2  3             b3  b7  b11  b15
 *        3  1  1  2             b4  b8  b12  b16
 * 
 * @param state 
 */
void mixColumn (byte *state)
{
    byte res1, res2, res3, res4;

    for(int i = 0; i < 16; i += 4){    
      res1 = mult(state[i], 2) ^ mult(state[i+1], 3) ^ state[i+2] ^ state[i+3];
      res2 = state[i] ^ mult(state[i+1], 2) ^ mult(state[i+2], 3) ^ state[i+3];
      res3 = state[i] ^ state[i+1] ^ mult(state[i+2], 2) ^ mult(state[i+3], 3);
      res4 = mult(state[i], 3) ^ state[i+1] ^ state[i+2] ^ mult(state[i+3], 2);

      state[i] = res1;
      state[i+1] = res2;
      state[i+2] = res3;
      state[i+3] = res4;
    }
}

/**
 * @brief Make a shift on 4 byte (in place)
 *        
 *        1, 2, 3, 4 -> 2, 3, 4, 1
 * 
 * @param state 
 */
void rotWord (byte state[4])
{
    byte temp;
    temp = state[0];
    state[0] = state[1];
    state[1] = state[2];
    state[2] = state[3];
    state[3] = temp;
}

/**
 * @brief Apply the S-Box on the 4 bytes (in place)
 * 
 * @param state 
 */
void SubWord (byte state[4])
{
    for (int i = 0; i < 4; i++)
        state[i] = sBox[state[i]];
}

/**
 * @brief Apply the reverse S-Box on the 4 bytes (in place)
 * 
 * @param state
 */
void inverse_SubWord(byte state[4]){
  for(int i = 0; i < 4; ++i){
    state[i] = inverse_sBox[state[i]];
  }
}

/**
 * @brief Return the rcon for the currently round
 * 
 * @param round
 * @return byte
 */
byte Rcon (int round)
{
    return Rcon_table[round];
}

/**
 * @brief Apply the expansion key (in place)
 * 
 * @param key 
 * @param round 
 */
void expKey (byte *key, int round)
{
    byte ek[4] = {key[12], key[13], key[14], key[15]};
    rotWord(ek);
    SubWord(ek);
    
    key[0] ^= ek[0] ^ Rcon(round);
    key[1] ^= ek[1];
    key[2] ^= ek[2];
    key[3] ^= ek[3];

    key[4] ^= key[0];
    key[5] ^= key[1];
    key[6] ^= key[2];
    key[7] ^= key[3];

    key[8] ^= key[4];
    key[9] ^= key[5];
    key[10] ^= key[6];
    key[11] ^= key[7];

    key[12] ^= key[8];
    key[13] ^= key[9];
    key[14] ^= key[10];
    key[15] ^= key[11];

}
/**
 * @brief Recover k0 from k10
 *
 * @param key
 * @param round
 */
byte* recoverKey(byte* key){
  for(int i = 9; i > -1; i--){
   
    key[15] ^= key[11];
    key[14] ^= key[10];
    key[13] ^= key[9];
    key[12] ^= key[8];
    
    key[11] ^= key[7];
    key[10] ^= key[6];
    key[9] ^= key[5];
    key[8] ^= key[4];
    
    key[7] ^= key[3];
    key[6] ^= key[2];
    key[5] ^= key[1];
    key[4] ^= key[0];

    byte ek[4] = {key[12], key[13], key[14], key[15]};
    SubWord(ek);
    rotWord(ek);
    
    key[3] ^= ek[3];
    key[2] ^= ek[2];
    key[1] ^= ek[1];
    key[0] ^= ek[0] ^ Rcon(i);

  }
  return key;
}

/**
 * @brief Apply AES (in place)
 * 
 * @param state 
 * @param key 
 */
void aes (byte *state, byte *key)
{
    addRoundKey(state, key);
    expKey(key, 0);
    for (int i = 0; i < 9; i++)
    {
        byteSub(state);
        shiftRow(state);
        mixColumn(state);
        addRoundKey(state, key);
        expKey(key, i + 1);
    }

    byteSub(state);
    shiftRow(state);
    addRoundKey(state, key);
}

// int main(void)
// {
//     byte input[16] = {0x32, 0x43, 0xf6, 0xa8, 0x88, 0x5a, 0x30, 0x8d, 0x31,
//                       0x31, 0x98, 0xa2, 0xe0, 0x37, 0x07, 0x34};
//     byte cipher_key[16] = {0x2b, 0x7e, 0x15, 0x16, 0x28, 0xae, 0xd2, 0xa6, 
//                            0xab, 0xf7, 0x15, 0x88, 0x09, 0xcf, 0x4f, 0x3c};
//     fct_debug(cipher_key);
//     aes(input, cipher_key);
//     //39 25 84 1d 02 dc 09 fb dc 11 85 97 19 6a 0b 32
//     // fct_debug(input);
//     //k10 = d0 14 f9 a8 c9 ee 25 89 e1 3f 0c c8 b6 63 0c a6
//     byte k10[16] = {0xd0, 0x14, 0xf9, 0xa8, 0xc9, 0xee, 0x25, 0x89, 0xe1, 0x3f, 0x0c, 0xc8, 0xb6, 0x63, 0x0c, 0xa6};
    
//     recoverKey(k10);
//     fct_debug(k10);

//     return EXIT_SUCCESS;
// }
