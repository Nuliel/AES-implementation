#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

#include "stats.h"

/**
 * @brief Free the structure1
 * 
 * @param state struct result
 */
void free_res(result r)
{
  free(r->deviation);
  free(r->mean);
  free(r);
}

int *offset_compute(float **data, int samples, int traces)
{
  // parameters
  double threshold = -0.14;
  int *offset_encrypt = malloc(traces * sizeof(int));
  for (int i = 0; i < traces; i++)
    offset_encrypt[i] = -1;
  for (int sample = 0; sample < samples; sample++)
    for (int trace = 0; trace < traces; trace++)
      if (offset_encrypt[trace] == -1 && data[trace][sample] < threshold)
        offset_encrypt[trace] = sample;

  // for (int trace = 0; trace < traces; trace++)
  //   printf("Offset %d: %d\n", trace, offset_encrypt[trace]);
  return offset_encrypt;
}

/**
 * @brief Computes the mean and the deviation of data
 * 
 * @param data Raw input data
 * @param sample Number of samples

 * @param traces Number of traces
 * @param offset Pointer to array of offsets (for countermeasure 0), use NULL instead
 */
result stats_compute(float **data, int samples, int traces, int *offset)
{
  // initialize structures
  result r = malloc(sizeof(struct res));
  r->mean = calloc(samples, sizeof(double));
  r->deviation = calloc(samples, sizeof(double));
  for (int sample = 0; sample < samples; sample++)
  {
    for (int trace = 0; trace < traces; trace++)
    {
      if (offset != NULL)
      {
        if (sample + offset[trace] < samples)
        {
          r->mean[sample] += data[trace][sample + offset[trace]];
          r->deviation[sample] += (data[trace][sample + offset[trace]] * data[trace][sample + offset[trace]]);
        }
      }
      else
      {
        r->mean[sample] += data[trace][sample];
        r->deviation[sample] += (data[trace][sample] * data[trace][sample]);
      }
    }
    r->mean[sample] *= (double)1 / traces;
    r->deviation[sample] *= (double)1 / traces;
    r->deviation[sample] -= (r->mean[sample] * r->mean[sample]);
    r->deviation[sample] = sqrt(r->deviation[sample]);
  }
  return r;
}

/**
 * @brief Compute pearson coefficient
 * 
 * @param cov
 * @param x Structure of two arrays. First array contains the mean,
 *          the second contains the deviation for a specific key
 *          hypothesis
 * @param y Structure of two arrays. First array contains the mean,
 *          the second contains the deviation for a specific trace
 *          of consumption.
 * @param size_N number of traces
 * @param i i.th value of the array.
 * @param key
 */
double pearson(double cov, result x, result y, int size_N, int i, int key)
{
  if (y->deviation[i] != 0)
    return ((double)1 / size_N * cov - x->mean[key] * y->mean[i]) /
           (x->deviation[key] * y->deviation[i]);
  return 0;
}

/**
 * @brief Compute the correlation matrix of dimension:
 * 256 x nb_of_samples. Compute the correlation beetween the traces and
 * the model
 * /!\ you can try to plot this correlations
 * 
 * @param traces An array of consumption traces
 * @param model An array of key hypothesis
 * @param size_d number of samples per trace
 * @param size_N number of traces
 */
double **correlation(float **traces, float **model, int size_d,
                     int size_N, result res_traces, int *offset,
                     FILE *fd)
{
  // double cov = 0;
  // result res_traces = stats_compute(traces, size_d, size_N);
  result res_model = stats_compute(model, 256, size_N, NULL);

  double **output = malloc(256 * sizeof(double *));
  for (int key = 0; key < 256; key++)             // for each all guesses
    output[key] = calloc(size_d, sizeof(double)); // all correlation

  /* for (int key = 0; key < 256; key++){ // for all guesses  */
  /*   for (int u = 0; u < size_d; u++) // for all samples */
  /*     { */
  /*       cov = 0; // init the sum of the */
  /*       for (int i = 0; i < size_N; i++) // for each traces */
  /*         if (model[i][key] != 0) */
  /*           cov += traces[i][u] * model[i][key]; // compute the product */

  /*       // compute the pearson coeff btw the model and the current sample */
  /*       output[key][u] = pearson(cov, res_model, res_traces, size_N, u, key); */
  /*     } */
  /* } */

  // Optimization
  for (int key = 0; key < 256; key++)
  { // for each key guess
    double cov[size_d];
    memset(cov, 0, size_d * sizeof(double));
    for (int trace = 0; trace < size_N; trace++)
    { // for each traces
      if (model[trace][key] != 0)
      { // check if the model is not null
        for (int feature = 0; feature < size_d; feature++)
        { // for each samples
          if (offset == NULL)
            cov[feature] += traces[trace][feature] * model[trace][key];
          else if (feature + offset[trace] < size_d)
            cov[feature] += traces[trace][feature + offset[trace]] * model[trace][key];
        }
      }
    }
    for (int feature = 0; feature < size_d; feature++)
    { // for each samples
      output[key][feature] = pearson(cov[feature], res_model, res_traces, size_N, feature, key);
      fprintf(fd, "%f", fabs(output[key][feature]));
    }
    fputc('/', fd);
  }

  //free_res(res_traces);
  free_res(res_model);

  return output;
}

/**
 * @brief return the key guess with the highest ABSOLUTE value
 * correlation value (take as input the output of 'correlation')
 * 
 * @param correlations
 * @param nb_keys The amount of keys, or the first dimension of the array
 */
struct pair candidate(double **data, int nb_samples, int nb_keys)
{
  double max_array[256];
  for (int i = 0; i < nb_keys; i++)
  {
    max_array[i] = data[i][0];
    for (int j = 0; j < nb_samples; j++)
    {
      if (fabs(max_array[i]) < fabs(data[i][j]))
      {
        max_array[i] = data[i][j];
      }
    }
  }
  int lmax = 0;
  for (int i = 0; i < 256; i++)
    if (fabs(max_array[lmax]) < fabs(max_array[i]))
      lmax = i;
  int lmax1;
  if (lmax == 0)
    lmax1 = 1;
  else
    lmax1 = 0;
  for (int i = 0; i < 256; i++)
    if (i != lmax && fabs(max_array[lmax1]) < fabs(max_array[i]))
      lmax1 = i;
  struct pair res;
  res.first = lmax;
  res.second = lmax1;
  return res;
}
