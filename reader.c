#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

#include "reader.h"
size_t ret;
void free_struct_traces (struct traces_struct traces){
   /*******************************************************************************
   * free_struct_traces
   * free the malloc memory of a traces_struct
   ******************************************************************************/
  for (int i = 0; i < traces.nb_of_traces; i++){
    free (traces.traces [i]);
  }
  free (traces.traces);
}

void free_struct_data (struct data_struct data){
  /*******************************************************************************
   * free_struct_data
   * free the malloc memory of a data_struct
   ******************************************************************************/
  for (int i = 0; i < data.nb_of_traces; i++){
    free (data.data [i]);
  }
  free (data.data);
}


struct traces_struct load_traces (const char *path){
  /*******************************************************************************
   * load_traces
   * read the trace stored in "path". Data are stored as follow
   * [D: nb of samples - 4 bytes][Q:nb of traces - 4 bytes] [D x Q - 4 bytes]
   *
   * return traces_struct (float**): traces [0]      -> D samples
   *                                 traces [1]      -> D samples
   *                                        ....
   *                                 traces [Q - 1]  -> D samples
   ******************************************************************************/
  FILE *f = fopen (path, "rb");

  struct traces_struct traces;

  ret = fread (&traces.nb_of_samples, 4, 1, f);
  ret = fread (&traces.nb_of_traces, 4, 1, f);

  printf ("%d traces of %d samples loaded\n",
          traces.nb_of_traces, traces.nb_of_samples);

  traces.traces = malloc (traces.nb_of_traces*sizeof (float *));
  for (int i = 0; i < traces.nb_of_traces; i++){
    traces.traces [i] = malloc (traces.nb_of_samples*sizeof (float));
    ret = fread (traces.traces [i], 4, traces.nb_of_samples, f);
  }

  fclose (f);

  return traces;
}


struct data_struct load_data (const char *path){
  /*******************************************************************************
   * load_data
   * read the data stored in "path". Data are stored as follow
   * [B: nb of bytes - 4 bytes][Q:nb of traces - 4 bytes] [B x Q - 1 bytes]
   *
   * return data_struct (uint8**): traces [0]      -> B bytes
   *                               traces [1]      -> B bytes
   *                                        ....
   *                               traces [Q - 1]  -> B bytes
   ******************************************************************************/
  FILE *f = fopen (path, "rb");

  struct data_struct data;

  ret = fread (&data.nb_of_bytes, 4, 1, f);
  ret = fread (&data.nb_of_traces, 4, 1, f);

  printf ("%d data of %d values loaded\n",
          data.nb_of_traces, data.nb_of_bytes);

  data.data = malloc (data.nb_of_traces*sizeof (uint8_t *));
  for (int i = 0; i < data.nb_of_traces; i++){
    data.data [i] = malloc (data.nb_of_bytes*sizeof (uint8_t));
    ret = fread (data.data [i], 1, data.nb_of_bytes, f);
  }

  fclose (f);

  return data;
}

// /*******************************************************************************/
// int main () // (int argc, char *argv[])
// /*******************************************************************************/
// {

//   struct traces_struct traces = load_traces ("../../data/hardware_traces_k_unknown/traces.bin");
//   struct data_struct plaintext = load_data ("../../data/hardware_traces_k_unknown/plaintext.bin");
//   struct data_struct ciphertext = load_data ("../../data/hardware_traces_k_unknown/ciphertext.bin");

//   free_struct_traces (traces);
//   free_struct_data (plaintext);
//   free_struct_data (ciphertext);
// }
