#ifndef STATS_H
#define STATS_H

struct res
{
    double *mean;
    double *deviation;
};

struct pair
{
    int first;
    int second;
};

typedef struct res *result;

void free_res (result r);
result stats_compute(float **data, int samples, int traces, int *offset);

double **correlation (float **traces, float **model, int size_d,
                      int size_N, result res_traces, int *offset, FILE* fd);
struct pair candidate(double** data, int nb_traces, int nb_keys);
int *offset_compute(float **data, int samples, int traces);
#endif
