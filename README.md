On this repo, you will find:

- an implementation of AES 128 in C
- an implementation of CPA for first and last round, and for *random delay* countermeasure
- a report about Side Channel Attacks