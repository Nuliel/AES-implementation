% \documentclass[10pt,a4paper]{article}
% \usepackage[utf8]{inputenc}
% \usepackage[english]{babel}
% \usepackage{graphicx}
% \usepackage{tikz}
% \begin{document}
\section{Countermeasures}
We have three countermeasures to evaluate, sorted by order of difficulty: random delay, shuffling and masking.

\subsection{Random delay}
The first one is \emph{random delay}. 
The main idea is to desynchronize traces so we can't compute mean and standard deviation without resynchronize traces. 
So we need to resynchronize traces before applying CPA.

In the implementation we evaluated, there is a loop before the encryption with a variable incremented and the loop ends when we reach a random value. 
This random value can be different for each trace. 

Let's see the first trace. 
\begin{center}
    \includegraphics[width=120mm]{counter_measures/mean_trace_countermeasure0.png}
\end{center}

We can see that there is a peak to $-0.14$ around sample 800, and we can observe this peak on all traces. 
It seems that this peak isn't far from end of random delay and from the beginning of AES. 
So in order to synchronize traces, we can set this peak as the first sample. 
It doesn't matter if we consider that our first sample doesn't fit exactly to the beginning of AES: we just need traces to be synchronized without removing the leakage part.

We have also to truncate traces so each trace has the same number of samples. 
If we let them, it can give some problems on the computation of the mean and standard deviation because there are fewer samples at the end.
Without truncating traces, it could mislead the CPA algorithm.

With these two steps, we succeeded in recovering the good key. 

\subsection{Shuffling}
The idea of this countermeasure is to use random values to mix operations in each block. 
For example, in the implementation, instead of performing AddRoundKey from first to last byte, 
we just do all operations of AddRoundKey but not in the same order. 
In the case of the implementation we worked on, it's a shift of operations inside each block. 
We do this for all parts of AES, so MixColumn, ShiftRow, SubBytes and AddRoundKey.
We can't apply CPA on these traces because for each trace, we have different random values, 
so each operation isn't synchronized.

We can see on traces \ref{trace_shuffling} that operations on ShiftRow seem not to be 
in same order for these two traces. Using the implementation, we can see that the 
chip doesn't manipulate bytes 0, 4, 8, 12 but all others bytes are manipulated by 
ShiftRow. There isn't enough entropy because there are just 3 permutations 
for ShiftRow (start by row 1 or 2 or 3). So we can recover all bytes except bytes 0, 4, 8 and 12. 
To recover last bytes, we can bruteforce them, 
it's possible because it represents at maximum $256^4 = 2^{32}$ computations of AES.

\begin{figure}[!h]
	\centering
    \includegraphics[scale=0.40]{counter_measures/traces_countermeasure1.png}
    \caption{Impact of Shuffling on traces}
    \label{trace_shuffling}
\end{figure}

Another technique is to use the AddRoundKey part or any part where all bytes are manipulated and 
where we can clearly see where a byte is manipulated. Then we can create 16 subtraces for each byte, 
and we sum all of them so we obtain a new trace. We can now apply CPA on it because when we try 
to recover one byte, others bytes are considered as noise.

\subsection{Masking}
The idea of this countermeasure is to avoid manipulating data directly. 

For this, we use random masks. The number of masks for an operation depends on the order of the attack. 
For instance, an attack of first order needs two leakage parts to recover the master key. In this case, 
we need one mask $m_1$, and we don't manipulate $x$ directly but we manipulate $x \oplus m_1$ and $m_1$.
We use one mask that is different for each operation. We can also use one mask per byte, but in the 
implementation evaluated, we don't have that.

For ShiftRow, MixColumn and AddRoundKey, it's easy to do this because these operations are linear, so
\begin{equation}
    \forall x, \forall m, f(x \oplus m) = f(x) \oplus f(m)
\end{equation}
Then we can compute $f(x \oplus m)$ and XOR it to $f(m)$, so we obtain $f(x)$ without manipulating $x$ directly.

The main problem is for SubBytes which is not linear.
For this, we can precompute SBOX $S'$ especially for each mask in order to have
\begin{equation}
    \forall y, S'(y \oplus m) = S(y) \oplus m'
\end{equation}
where $m'$ is the output of $m$ through SBOX $S$.

So we have this computation:
\begin{center}
    \begin{tikzpicture}[
        roundnode/.style={circle, draw=black!30, fill=black!5, very thick, minimum size=7mm},
        ]
        %Nodes
        \node[roundnode] (x) at (0, 0) {$x$};
        \node[roundnode] (xor1) at (1, 0) {$+$};
        \node            (m) at (1, 1) {$m$};
        \node[roundnode] (xor2) at (2, 0) {$+$};
        \node            (k) at (2, 1) {$k$};
        \node[roundnode] (S1) at (3, 0) {$S'$};
        \node            (result) at (4.5, 0) {$z \oplus m'$};
        
        %Lines
        \draw[->] (x.east) -- (xor1.west);
        \draw[->] (m.south) -- (xor1.north);
        \draw[->] (xor1.east) -- (xor2.west);
        \draw[->] (k.south) -- (xor2.north);
        \draw[->] (xor2.east) -- (S1.west);
        \draw[->] (S1.east) -- (result.west);
    \end{tikzpicture}
\end{center}
The idea to defeat the countermeasure is to combine each mask and masked data to unmask sensitive data.
When we see the first trace \ref{trace_mask}, we can see two MixColumn: one for masked data and one for mask.

\begin{figure}[!h]
	\centering
    \includegraphics[scale=0.40]{counter_measures/trace_countermeasure2.png}
    \caption{Impact of masking on traces}
    \label{trace_mask}
\end{figure}

To combine mask and masked data, we use centered product $(X_{i,a} - \mu_a)(X_{i,b} - \mu_b)$
where $\mu_a$ is the mean of sample a, $X_{i,a}$ is the sample a of trace i. 
Now we can apply CPA on this new set. The main problem of this is when we use centered product, we 
put noise to square, so if it was a higher order countermeasure, we could have problems with noise.

\subsection{Others countermeasures}
In order to avoid evaluations even with countermeasures, we can use many countermeasures at same time, 
and we can apply some hardware modifications like using dual rail or using shield to make attacks harder \cite{DBLP:conf/asiacrypt/LomnePR13}.
We can also use a higher order for masking countermeasure in order to increase the complexity of the attack.
% \end{document}