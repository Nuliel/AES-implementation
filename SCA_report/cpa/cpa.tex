% \documentclass[a4paper, 12pt]{article}
% \usepackage[T1]{fontenc}
% \usepackage[utf8]{inputenc}
% \usepackage{listings}
% \usepackage{fullpage}
% \usepackage{graphicx}
% \title{Final report:\\ Correlation Power Analysis}
% \author{Vincent AUBRIL, Maël FRAZAO}
% \begin{document}
\section{Correlation Power Analysis (CPA)}

\subsection{Explanation}

Correlation power analysis is one of the most famous side channel attack (SCA). 
It is based on the correlation and in particular on the Pearson coefficient. 
In our case we use the power consumption of a chip during the encryption (figure: \ref{power_comsuption}). 
The goal here is to make the correlation between a model and the side channel leak and take the guessed key with the highest score. 
The side channel leak is composed of traces and each trace is divided in samples. 
Traces correspond to an encryption of a given plaintext and sample is a measure of the power consumption.
Here for efficiency, we use a \emph{divide-and-conquer} approach. 
We don't work on the key itself but byte by byte.

\begin{figure}[!h]
	\centering
    \includegraphics[scale=0.30]{cpa/power_software.png}
    \caption{Power consumption during an AES encryption on a software implementation}
    \label{power_comsuption}
\end{figure}

On figure \ref{power_comsuption} we can see variations of the chip power consumption for every measure.
We have the first 8 rounds of the AES encryption because we can see 8 repeated patterns that correspond 
of them.
\subsection{Pearson coefficient}

Pearson coefficient measures the linear correlation between two sets.
This is the covariance divided by the product of the standard deviation of the two sets.
The mathematical formula is the following:

\begin{center}
\[\rho_{X,Y} = \frac{cov(X,Y)}{\sigma_X \sigma_Y}\]
\end{center}
with 
\[
	cov(X, Y) = \frac{\sum_{i=1}^{N}x_iy_i - \frac{1}{N} \left(\sum_{i=1}^N x_i\right)\left(\sum_{i=1}^N y_i\right)}{N}
\]
Here for this evaluation, we have two sets: the side channel leakage and the leakage model.
In our case, more the absolute value of the Pearson coefficient is close to 1 more the guess key can be the real key.
With all this information we are able to perform the evaluation and recover the key.
But currently we just have the side channel leakage, we have to compute the leakage model.

\subsection{Leakage model}

The leakage model is one of the most tricky parts of the evaluation.
In order to choose this model we need to know how the register that contains the current block is erased.
In fact, when this register is erased by the new value, a leak of information appears.
So if you know how the register is updated you know which leakage model use.
In this project we use two types of leakage models: a leakage model at the first round using Hamming weight and a second one at the last round of AES encryption using Hamming distance. 
During this project, we use a specific leakage model for each type of implementation, but in the application all leakage models can be used on every implementation. 
It just depends on where the leakage is.

\subsubsection{First round}

In order to perform the evaluation on the first round, the update of the register needs to be just after the \emph{SubBytes} operation. 
In this case, the equation of the model is the following:

\begin{center}
	\begin{lstlisting}
	pop_count(inverse_sbox[target_cleartext ^ key])
	\end{lstlisting}
\end{center}

Computing a Hamming distance between $x$ and 0 is equivalent to compute the Hamming weight of $x$.
Here we assume the value of the register is to 0 because the value before the update is not essential.
So if the value of the register is to zero before the update, we have $w$ the Hamming weight and $d$ the Hamming distance as:
\[d(x, 0) = w(x - 0) = w(x)\]

\subsubsection{Last round}

For the last round, you need to inverse the sbox of the element xor with the guessed key for recover the state before the sbox. 
Then you have to xor the ciphertext with the result of the last compute, on which you apply the \emph{ShiftRow} operation 
for realign bytes.

\begin{lstlisting}
hamming_dist(inverse_sBox[target_ciphertext ^ key], pred_state)
\end{lstlisting}

Here we use the Hamming distance because the register is not fully updated (some bits keep the same value) and the last value of the register is a part of the encryption so we can't ignore it.

\subsubsection{How to choose a leakage model?}

It's important to choose a correct leakage model to perform this evaluation and it's a bit harder when you don't have any experience. 
But one way to know which leakage model to use is to display the graph of each trace. 
After some experiments you can choose the best one. 
As we can see in figure \ref{leak_first}, for the correlation on software traces and the leakage model in the first round,
we have some big variations at the beginning. 
This is the leakage of the key bytes during encryption.
But if you have access to the right key you can just compare it with your guess key. 
If you look for the hardware implementation (figure: \ref{leak_last}) the key leakage is at the end of the encryption.

If you don't choose the right leakage model, we can't recover the correct key by finding the highest correlation.
On the figure \ref{leak_fail}, we can not see a real demarcation on the different correlation of the 16 bytes 
like in figure \ref{leak_first} and \ref{leak_last}.

\begin{figure}[!h]
	\centering
    \includegraphics[scale=0.30]{cpa/leak_first.png}
    \caption{Correlation for software traces}
    \label{leak_first}
\end{figure}

\begin{figure}[!h]
	\centering
    \includegraphics[scale=0.30]{cpa/leak_last.png}
    \caption{Correlation for hardware traces}
    \label{leak_last}
\end{figure}

\begin{figure}[!h]
	\centering
    \includegraphics[scale=0.30]{cpa/fail_leakage_model_hard_first.png}
    \includegraphics[scale=0.30]{cpa/fail_leakage_model_soft_last.png}
    \caption{Correlation with wrong leakage model the leakage model}
    \label{leak_fail}
\end{figure}

\subsection{Implementation}

In order to prevent multiple reads in the table, we perform cumulative computation. 
This idea allows to have better time
complexity for both model and leakage value when we compute mean and standard deviation.

So the mean and standard deviation are computed at the same time. In fact, 
the size of the two tables is big (for the software it is 9.996.000 values). 
We use the following formula for the standard
deviation:

\[
	\sigma = \sqrt{\left(\frac{1}{N}\sum_{i = 1}^{N}x_{i}^2\right) - \mu^2}
\]

For the remainder we follow the algorithm given by the paper Behind the Scene of side Channel Attacks. 
The only subtlety is in the experimentation on the last round. 
At the end, the recover key is the last round key ($k_{10}$). 
So we just have to invert the exponentiation key for recover the first key used for the encryption ($k_0$).

% \end{document}