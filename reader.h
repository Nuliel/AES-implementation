#ifndef READER_H
#define READER_H

#include <stdint.h>

struct traces_struct {
  float **traces;
  int nb_of_traces;
  int nb_of_samples;
};

struct data_struct {
  uint8_t **data;
  int nb_of_traces;
  int nb_of_bytes;
};

void free_struct_traces (struct traces_struct traces);
void free_struct_data (struct data_struct data);
struct traces_struct load_traces (const char *path);
struct data_struct load_data (const char *path);

#endif