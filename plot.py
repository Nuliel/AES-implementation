#!/bin/env python3

import matplotlib.pyplot as plt

"""
with open("correlation.data", "r") as file_in:
    # first key
    temp = file_in.readline()[:-2]
    l = [float(elt) for elt in temp.split(',')]
    plt.plot(range(0, 5*len(l),5), l, '.')
    # other keys
    if temp != "\n":
        l = [float(elt) for elt in temp.split(',')]
        plt.plot(range(0, 5*len(l),5), l,  '<')
plt.show()
"""

for i in range(16):
    filename = "data_plot/" + str(i).zfill(2)
    print(filename)
    fd = open(filename + ".data", "r")
    temp = fd.readline()[:-2]
    l = [float(elt) for elt in temp.split(',')]
    plt.plot(range(0, 5*len(l),5), l, 'r.')
    plt.savefig(filename + ".png")
    plt.clf()
    #print(temp)
    #print(fd.read())
    fd.close()